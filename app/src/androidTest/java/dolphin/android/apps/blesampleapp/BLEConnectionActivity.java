package dolphin.android.apps.blesampleapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/**
 * reference code from AVerMedia Live in Five (O110/BC310) by Mike Yeh
 */
public class BLEConnectionActivity extends Activity implements OnClickListener {
    //	private final static String UUID_R_HEX_POWER_STATE = "4c08ffa1-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_R_HEX_STREAM_STATE = "4c08ffa2-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_R_HEX_OCCUPY_STATE = "4c08ffa3-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_RN_IP = "4c08ffa4-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_N_DEC_WIFI_STATE = "4c08ffa5-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_RW_SET_SSID_1 = "4c08ffa6-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_RW_SET_SSID_2 = "4c08ffa7-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_RW_SET_PW_1 = "4c08ffa8-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_RW_SET_PW_2 = "4c08ffa9-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_RW_SET_PW_3 = "4c08ffaa-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_RW_SET_PW_4 = "4c08ffab-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_RW_SET_PROT = "4c08ffac-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_R_OCC_DEVICE = "4c08ffad-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_RW_REG_OCC_DEVICE = "4c08ffae-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_R_LIVE_URL_1 = "4c08ffaf-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_R_LIVE_URL_2 = "4c08ffb0-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_R_LIVE_URL_3 = "4c08ffb1-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_R_LIVE_URL_4 = "4c08ffb2-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_R_LIVE_URL_5 = "4c08ffb3-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_R_HEX_CDN_TYPE = "4c08ffb4-0514-11e6-b512-3e1d05defe78";
    private final static String UUID_RN_SESSION = "4c08ffb5-0514-11e6-b512-3e1d05defe78";


    private final static long SCAN_PERIOD = 5000;
    private final static int REQUEST_ENABLE_BT = 99;

    private final static int WIFI_STATE_SET_CONFIG_FAIL = 201;
    private final static int WIFI_STATE_SSID_NOT_FOUND = 202;
    private final static int WIFI_STATE_SSID_FOUND = 210;
    private final static int WIFI_STATE_AUTH_FAIL = 203;
    private final static int WIFI_STATE_TIME_OUT = 208;
    private final static int WIFI_STATE_SSID_CONNECTED = 211;

    /**
     * Switch UI
     **/
    // UEC Flow 03_2.13
    private final static int SWITCH_FINDING_BC310_UI = 1;
    // UEC Flow 03_2.20
    private final static int SWITCH_STANDALONE_BC310_UI = 2;
    // UEC Flow 03_2.19
    private final static int SWITCH_ALREADY_CONNECT_BC310_UI = 3;
    // UEC Flow 03_2.14
    private final static int SWITCH_SEARCH_BC310_UI = 4;
    // UEC Flow 03_2.18
    private final static int SWITCH_CONNECTION_BC310_UI = 5;
    // UEC Flow 03_3.1
    private final static int SWITCH_DISCONNECTED_BC310_UI = 6;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mGatt;
    private final HashMap<String, BluetoothGattCharacteristic> characteristicMap = new HashMap<String, BluetoothGattCharacteristic>();
    private final HashMap<String, BluetoothDevice> mBleDevicesHashMap = new HashMap<String, BluetoothDevice>();
    private final ArrayList<String> mBc310SsidArrayList = new ArrayList<String>();

    /**
     * BC310 Parameter
     **/
    // AP Router Parameter
    private String mStringApSsid = "";
    private int mIntApSecurity;
    private int mIntPwLength;

    // WiFi Hotspot Parameter
    private String mStringHotspotSsid = "";
    private String mStringHotspotPw = "";
    private int mIntHotspotSecurity;

    /**
     * BC310 Parameter
     **/
    private int mIntBc310SessionKey = -1;
    private String mStringBc310ApSsid = "";
    private String mStringBc310ClientIp = "";
    private String mStringALDeviceName;
    private boolean isBc310ClientStreaming = false;
    private String mStringErrorMessage;

    // CDN URL Parameter
    private String mStringCdnUrl = "";
    // Mobile Network Type (WiFi/3G/NONE)
    private int mIntMobileType = -1;

    private boolean isFromNFC = false;

//	/** DialogFragment **/
//	private Bc310SsidDialogFragment mBc310SsidDialog;
//	private ApPwDialogFragment mApPwDialog;

    /**
     * Include Layout
     **/
    // Finding Mode
    private RelativeLayout mLayoutFindingMode;
    private AnimationDrawable mFindingAnimation;
    private TextView mFindingDescribe;
    // Connecting Mode
    private RelativeLayout mLayoutConnectingMode;
    private AnimationDrawable mConnectingAnimation;
    // Standalone Mode
    private RelativeLayout mLayoutStandaloneMode;
    ImageView mPowerLightImageView;
    private LinearLayout mYoutubeBtn;
    private LinearLayout mLivehouseinBtn;
    private LinearLayout mTwitchBtn;
    private LinearLayout mUstreamBtn;
    private LinearLayout mCustomBtn;
    // Search Mode
    private RelativeLayout mLayoutSearch;
    private LinearLayout mSearchBtn;
    private LinearLayout mTryOtherConnectBtn;
    private LinearLayout mQuestionBtn;
    private TextView mTextViewSearchTitle;
    // Disconnected Mode
    private RelativeLayout mLayoutDisconnected;
    // Disconnected Mode
    private RelativeLayout mLayoutAlreadyConnect;
    private TextView mTextViewACTitle;
    private TextView mTextViewACDeviceName;

    // Timer
    private Timer timerPowerLight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // BLE and WiFi Same UI - Full Screen no Title bar
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		setContentView(R.layout.activity_bc310_connection_wifi);
//		initView();
//		initData();
        initBLE();
    }

//	private void initView() {
//		// inclue layout : layout_bc310_finding_mode (flow 0.7)
//		mLayoutFindingMode = (RelativeLayout) findViewById(R.id.layout_bc310_finding);
//		ImageView mFindingImageView = ((ImageView) mLayoutFindingMode.findViewById(R.id.imageView_bc310_finding));
//		mFindingImageView.setBackgroundResource(R.drawable.bc310_finding);
//		mFindingDescribe = ((TextView) mLayoutFindingMode.findViewById(R.id.textView_bc310_finding_describe));
//		mFindingDescribe.setVisibility(View.INVISIBLE);
//		mFindingAnimation = (AnimationDrawable) mFindingImageView.getBackground();
//		mFindingAnimation.start();
//
//		// include layout : layout_bc310_wifi_search (flow 2.20)
//		mLayoutSearch = (RelativeLayout) findViewById(R.id.layout_wifi_search);
//		((ImageButton) mLayoutSearch.findViewById(R.id.imageButton_wifi_search_back)).setOnClickListener(this);
//		mSearchBtn = ((LinearLayout) mLayoutSearch.findViewById(R.id.button_wifi_connect_search));
//		mSearchBtn.setOnClickListener(this);
//		mTryOtherConnectBtn = ((LinearLayout) mLayoutSearch.findViewById(R.id.button_wifi_try_other_connection));
//		mTryOtherConnectBtn.setOnClickListener(this);
//
//		mTextViewSearchTitle = (TextView) mLayoutSearch.findViewById(R.id.textView_wifi_search_title);
//		mTextViewSearchTitle.setText(getString(R.string.wifi_connection_title_live_caster_unable_to_connect));
//		mQuestionBtn = ((LinearLayout) mLayoutSearch.findViewById(R.id.button_wifi_search_question));
//		mQuestionBtn.setOnClickListener(this);
//
//
//		// inclue layout : layout_bc310_connecting_mode (flow 2.14)
//		mLayoutConnectingMode = (RelativeLayout) findViewById(R.id.layout_bc310_connecting);
//		ImageView mConnectingImageView = ((ImageView) mLayoutConnectingMode
//				.findViewById(R.id.imageView_bc310_connecting));
//		mConnectingImageView.setBackgroundResource(R.drawable.bc310_connecting);
//		mConnectingAnimation = (AnimationDrawable) mConnectingImageView.getBackground();
//
//		// include layout : layout_bc310_wifi_standalone_mode (flow 2.15)
//		mLayoutStandaloneMode = (RelativeLayout) findViewById(R.id.layout_wifi_standalone_mode);
//		((ImageButton) mLayoutStandaloneMode.findViewById(R.id.imageButton_wifi_connect_back)).setOnClickListener(this);
//		mYoutubeBtn = ((LinearLayout) mLayoutStandaloneMode.findViewById(R.id.button_wifi_connect_go_to_youtube));
//		mYoutubeBtn.setOnClickListener(this);
//		mLivehouseinBtn = ((LinearLayout) mLayoutStandaloneMode
//				.findViewById(R.id.button_wifi_connect_go_to_livehousein));
//		mLivehouseinBtn.setOnClickListener(this);
//		mTwitchBtn = ((LinearLayout) mLayoutStandaloneMode.findViewById(R.id.button_wifi_connect_go_to_twitch));
//		mTwitchBtn.setOnClickListener(this);
//		mUstreamBtn = ((LinearLayout) mLayoutStandaloneMode.findViewById(R.id.button_wifi_connect_go_to_ustream));
//		mUstreamBtn.setOnClickListener(this);
//		mCustomBtn = ((LinearLayout) mLayoutStandaloneMode.findViewById(R.id.button_wifi_connect_go_to_custom));
//		mCustomBtn.setOnClickListener(this);
//		mPowerLightImageView = ((ImageView) mLayoutStandaloneMode.findViewById(R.id.imageView_bc310_power_light));
//		((LinearLayout) mLayoutStandaloneMode.findViewById(R.id.button_wifi_standalone_reconection))
//				.setOnClickListener(this);
//
//		// inclue layout : layout_bc310_disconnected (flow 2.10)
//		mLayoutDisconnected = (RelativeLayout) findViewById(R.id.layout_wifi_disconnected);
//		((ImageButton) mLayoutDisconnected.findViewById(R.id.imageButton_wifi_disconnected_back)).setOnClickListener(this);
//		((LinearLayout) mLayoutDisconnected.findViewById(R.id.button_wifi_reconnect)).setOnClickListener(this);
//
//		// inclue layout : layout_bc310_already_connect (flow 2.21)
//		mLayoutAlreadyConnect = (RelativeLayout) findViewById(R.id.layout_wifi_already_connect);
//		((ImageButton) mLayoutAlreadyConnect.findViewById(R.id.imageButton_wifi_already_connect_back))
//				.setOnClickListener(this);
//		((LinearLayout) mLayoutAlreadyConnect.findViewById(R.id.button_wifi_already_connect)).setOnClickListener(this);
//		mTextViewACTitle = ((TextView) mLayoutAlreadyConnect.findViewById(R.id.textView_wifi_already_connect_title));
//		mTextViewACDeviceName = ((TextView) mLayoutAlreadyConnect
//				.findViewById(R.id.textView_wifi_already_connect_device_name));
//	}
//
//	private void initData(){
//		if ((getIntent() != null) && (getIntent().getStringExtra(NFCConnectionActivity.BC310_AP_MODE_SSID) != null)
//				&& !(getIntent().getStringExtra(NFCConnectionActivity.BC310_AP_MODE_SSID)).equals("")) {
//			isFromNFC = true;
//			mStringBc310ApSsid = getIntent().getStringExtra(NFCConnectionActivity.BC310_AP_MODE_SSID);
//		}
//		if (ConnectionUtils.getConnectType(this) == ConnectivityManager.TYPE_MOBILE) {
//			mIntMobileType = ConnectivityManager.TYPE_MOBILE;
//			mStringHotspotSsid = ConnectionUtils.getHotspotSsid(this);
//			mStringHotspotPw = ConnectionUtils.getHotspotPassword(this);
//			mIntHotspotSecurity = ConnectionUtils.getHotspotProtocol(this);
//		} else {
//			mIntMobileType = ConnectivityManager.TYPE_WIFI;
//			mStringApSsid = ConnectionUtils.getWifiSsid(this);
//			mIntApSecurity = ConnectionUtils.getWifiSecurity(this);
//		}
//	}

    private void initBLE() {
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            return;
        }
        scanLeDevice();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disconnectDevice();
    }

    /******************** onClick *************************/
    @Override
    public void onClick(View v) {
//		switch(v.getId()){
//		case R.id.imageButton_wifi_connect_back:
//		case R.id.imageButton_wifi_search_back:
//		case R.id.imageButton_wifi_disconnected_back:
//		case R.id.imageButton_wifi_already_connect_back:
//			setResult(RESULT_CANCELED);
//			finish();
//			break;
//		case R.id.button_wifi_connect_go_to_youtube:
//		case R.id.button_wifi_connect_go_to_livehousein:
//		case R.id.button_wifi_connect_go_to_twitch:
//		case R.id.button_wifi_connect_go_to_ustream:
//			// Uri uri = Uri.parse(mStringCdnUrl);
//			if (mStringCdnUrl.equals("")) {
//				Toast.makeText(this, "****No CDN Live Url****", Toast.LENGTH_SHORT).show();
//			} else {
//				Uri uri = Uri.parse(mStringCdnUrl);
//				Intent it = new Intent(Intent.ACTION_VIEW, uri);
//				startActivity(it);
//			}
//			break;
//		case R.id.button_wifi_connect_search:
//			disconnectDevice();
//			scanLeDevice();
//			break;
//		case R.id.button_wifi_search_question:
//			mQuestionBtn.setVisibility(View.INVISIBLE);
//			mSearchBtn.setVisibility(View.INVISIBLE);
//			mTextViewSearchTitle.setText(getString(R.string.wifi_connection_title_try_other_methods));
//			break;
//		case R.id.button_wifi_try_other_connection:
//			Intent intentOth = new Intent(this, NFCConnectionActivity.class);
//			intentOth.putExtra(NFCConnectionActivity.RESULT_NAME_TRY_OTHER, "OTHER");
//			setResult(RESULT_CANCELED, intentOth);
//			finish();
//			break;
//		case R.id.button_wifi_reconnect:
//			mGatt = null;
//			scanLeDevice();
//			break;
//		case R.id.button_wifi_standalone_reconection:
//		case R.id.button_wifi_already_connect:
//			Intent intentRe = new Intent(this, NFCConnectionActivity.class);
//			intentRe.putExtra(NFCConnectionActivity.RESULT_NAME_RECONNECT, "RE");
//			setResult(RESULT_CANCELED, intentRe);
//			finish();
//			break;
//		}
    }

    /******************** onActivityResult *************************/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                scanLeDevice();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void scanLeDevice() {
        switchUi(SWITCH_FINDING_BC310_UI);
        // Clear Map Data
        mBleDevicesHashMap.clear();
        mBc310SsidArrayList.clear();
        // Start scanning
        mBluetoothAdapter.startLeScan(mLeScanCallback);
        // Stops scanning after a pre-defined scan period.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
                // Show BLE device by scan
                for (String ssid : mBleDevicesHashMap.keySet()) {
                    mBc310SsidArrayList.add(ssid);
                }

                if (mBc310SsidArrayList.size() > 0) {
                    if (isFromNFC) {
                        for (int i = 0; i < mBc310SsidArrayList.size(); i++) {
                            if (mBc310SsidArrayList.get(i).equals(mStringBc310ApSsid)) {
                                BluetoothDevice device = mBleDevicesHashMap.get(mBc310SsidArrayList.get(i));
                                connectToDevice(device);
                                return;
                            }
                        }
                        // Use NFC but Not Find BC310 - Show Re-Scan UI
                        switchUi(SWITCH_SEARCH_BC310_UI);
                    } else {
                        if (mBc310SsidArrayList.size() > 1) {
//							mBc310SsidArrayList.add(getString(R.string.bc310_connection_dialog_add_another_device));
//							showSsidDialog(getAdapter(mBc310SsidArrayList));
                        } else {
                            mStringBc310ApSsid = mBc310SsidArrayList.get(0);
                            BluetoothDevice device = mBleDevicesHashMap.get(mBc310SsidArrayList.get(0));
                            connectToDevice(device);
                        }
                    }
                } else {
                    // Show Re-Scan UI
                    switchUi(SWITCH_SEARCH_BC310_UI);
                }
            }
        }, SCAN_PERIOD);
    }

    /******************** BLE Scan / Connect / Disconnect Method ******************/
    /**
     * BLE Scan Callback
     */
    private final BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            if ((device.getName() != null) && device.getName().indexOf("BC310_") != -1) {
                mBleDevicesHashMap.put(device.getName(), device);
            }
        }
    };

    /**
     * BLE Connect to Device
     *
     * @param device
     */
    public void connectToDevice(BluetoothDevice device) {
        if (mGatt == null) {
            mGatt = device.connectGatt(this, false, gattCallback);
        }
    }

    /**
     * BLE Disconnect
     */
    private void disconnectDevice() {
        if (mGatt != null) {
            mGatt.disconnect();
            mGatt.close();
            mGatt = null;
        }
    }

//	/******************** showBC310SsidDialog & onSsidItemClick response ******************/
//	/**
//	 * @param adapter
//	 */
//	private void showSsidDialog(BaseAdapter adapter) {
//		mBc310SsidDialog = new Bc310SsidDialogFragment(this, adapter, new OnSsidItemClickListener() {
//			@Override
//			public void onSsidItemClick(AdapterView<?> parent, View view, int position, long id) {
//				if (position == mBc310SsidArrayList.size() - 1) {
//					// Re-search
//					scanLeDevice();
//				} else {
//					// Get BC310 AP SSID
//					mStringBc310ApSsid = mBc310SsidArrayList.get(position);
//					BluetoothDevice device = mBleDevicesHashMap.get(mBc310SsidArrayList.get(position));
//					connectToDevice(device);
//				}
//			}
//		});
//		mBc310SsidDialog.show(getFragmentManager(), "BC310_SSID");
//	}

//	private BaseAdapter getAdapter(List<String> ssidArrayList) {
//		BaseAdapter adapter = new Bc310SsidAdapter(this, ssidArrayList);
//		return (Bc310SsidAdapter) adapter;
//	}

//	/****************** showApPwDialog ******************/
//	/**
//	 * Show AP Password DialogFragment UEC flow 2.11
//	 *
//	 * @param adapter
//	 */
//	private void showApPwDialog(String apSsidName, int apSecurity) {
//		if (!this.isFinishing() && ConnectionUtils.getForegroundAppName(this)) {
//			if (mApPwDialog == null) {
//				mApPwDialog = new ApPwDialogFragment(this, new OnApPwClickListener() {
//					@Override
//					public void onApPwClick(String apSsid, String apPw, int apSecurity) {
//						mIntApSecurity = apSecurity;
//						writeApPW(apPw);
//					}
//				});
//			}
//			mApPwDialog.setSsidName(apSsidName);
//			mApPwDialog.setSecurity(apSecurity);
//			mApPwDialog.show(getFragmentManager(), "AP_PASSWORD");
//		}
//	}

//	/**
//	 * Dismiss Dialog
//	 */
//	private void dismissApPwDialog() {
//		if ((mApPwDialog != null) && mApPwDialog.isShowing()) {
//			mApPwDialog.dismiss();
//		}
//	}

//	/**
//	 * 0 = USTREAM 1 = YOUTUBE 2 = LIVE_HOUSE_IN 3 = OTHER
//	 *
//	 * @param int type
//	 */
//	private void showCdnType(final int type) {
//		runOnUiThread(new Runnable() {
//			public void run() {
//				switch (type) {
//				case CDNconfig.CDN_USTREAM:
//					mUstreamBtn.setVisibility(View.VISIBLE);
//					mYoutubeBtn.setVisibility(View.INVISIBLE);
//					mLivehouseinBtn.setVisibility(View.INVISIBLE);
//					mCustomBtn.setVisibility(View.INVISIBLE);
//					break;
//				case CDNconfig.CDN_YOUTUBE:
//					mUstreamBtn.setVisibility(View.INVISIBLE);
//					mYoutubeBtn.setVisibility(View.VISIBLE);
//					mLivehouseinBtn.setVisibility(View.INVISIBLE);
//					mCustomBtn.setVisibility(View.INVISIBLE);
//					break;
//				case CDNconfig.CDN_LIVE_HOUSE_IN:
//					mUstreamBtn.setVisibility(View.INVISIBLE);
//					mYoutubeBtn.setVisibility(View.INVISIBLE);
//					mLivehouseinBtn.setVisibility(View.VISIBLE);
//					mCustomBtn.setVisibility(View.INVISIBLE);
//					break;
//				case CDNconfig.CDN_OTHER:
//					mUstreamBtn.setVisibility(View.INVISIBLE);
//					mYoutubeBtn.setVisibility(View.INVISIBLE);
//					mLivehouseinBtn.setVisibility(View.INVISIBLE);
//					mCustomBtn.setVisibility(View.VISIBLE);
//					break;
//				}
//			}
//		});
//	}
//
//	private void finishActivity() {
//		Intent intent = new Intent(BLEConnectionActivity.this, O110MainActivityWithBc310.class);
//		intent.putExtra(NFCConnectionActivity.RESULT_NAME_IP, mStringBc310ClientIp);
//		intent.putExtra(NFCConnectionActivity.RESULT_NAME_CAN_PREVIEW, isBc310ClientStreaming);
//		intent.putExtra(NFCConnectionActivity.RESULT_NAME_SESSION_KEY, mIntBc310SessionKey);
//		intent.putExtra(NFCConnectionActivity.RESULT_NAME_ERROR_MESSAGE, mStringErrorMessage);
//		BLEConnectionActivity.this.setResult(RESULT_OK, intent);
//		finish();
//	}

    /******************** Change UI Method *************************/
    /**
     * @param mode
     */
    private void switchUi(final int mode) {
        runOnUiThread(new Runnable() {
            public void run() {
                switch (mode) {
                    case SWITCH_FINDING_BC310_UI:
                        mLayoutFindingMode.setVisibility(View.VISIBLE);
                        mFindingAnimation.start();
                        mConnectingAnimation.stop();
                        stopPowerLightAnimation();
                        mLayoutStandaloneMode.setVisibility(View.INVISIBLE);
                        mLayoutAlreadyConnect.setVisibility(View.INVISIBLE);
                        mLayoutSearch.setVisibility(View.INVISIBLE);
                        mLayoutConnectingMode.setVisibility(View.INVISIBLE);
                        mLayoutDisconnected.setVisibility(View.INVISIBLE);
                        break;
                    case SWITCH_STANDALONE_BC310_UI:
                        mLayoutFindingMode.setVisibility(View.INVISIBLE);
                        mLayoutStandaloneMode.setVisibility(View.VISIBLE);
                        startPowerLightAnimation();
                        mFindingAnimation.stop();
                        mConnectingAnimation.stop();
                        mLayoutAlreadyConnect.setVisibility(View.INVISIBLE);
                        mLayoutSearch.setVisibility(View.INVISIBLE);
                        mLayoutConnectingMode.setVisibility(View.INVISIBLE);
                        mLayoutDisconnected.setVisibility(View.INVISIBLE);
                        break;
                    case SWITCH_ALREADY_CONNECT_BC310_UI:
                        mLayoutFindingMode.setVisibility(View.INVISIBLE);
                        mLayoutStandaloneMode.setVisibility(View.INVISIBLE);
                        mLayoutAlreadyConnect.setVisibility(View.VISIBLE);
//					String title = String.format(
//							getResources().getString(R.string.wifi_connection_title_already_connect),
//							mStringBc310ApSsid);
//					mTextViewACTitle.setText(title);
                        mTextViewACDeviceName.setText(mStringALDeviceName);
                        mFindingAnimation.stop();
                        mConnectingAnimation.stop();
                        stopPowerLightAnimation();
                        mLayoutSearch.setVisibility(View.INVISIBLE);
                        mLayoutConnectingMode.setVisibility(View.INVISIBLE);
                        mLayoutDisconnected.setVisibility(View.INVISIBLE);
                        break;
                    case SWITCH_SEARCH_BC310_UI:
                        mLayoutFindingMode.setVisibility(View.INVISIBLE);
                        mLayoutStandaloneMode.setVisibility(View.INVISIBLE);
                        mLayoutAlreadyConnect.setVisibility(View.INVISIBLE);
                        mLayoutSearch.setVisibility(View.VISIBLE);
                        mFindingAnimation.stop();
                        mConnectingAnimation.stop();
                        stopPowerLightAnimation();
                        mLayoutConnectingMode.setVisibility(View.INVISIBLE);
                        mLayoutDisconnected.setVisibility(View.INVISIBLE);
                        break;
                    case SWITCH_CONNECTION_BC310_UI:
                        mLayoutFindingMode.setVisibility(View.INVISIBLE);
                        mLayoutStandaloneMode.setVisibility(View.INVISIBLE);
                        mLayoutAlreadyConnect.setVisibility(View.INVISIBLE);
                        mLayoutSearch.setVisibility(View.INVISIBLE);
                        mLayoutConnectingMode.setVisibility(View.VISIBLE);
                        mFindingAnimation.stop();
                        mConnectingAnimation.start();
                        stopPowerLightAnimation();
                        mLayoutDisconnected.setVisibility(View.INVISIBLE);
                        break;
                    case SWITCH_DISCONNECTED_BC310_UI:
//					dismissApPwDialog();
                        mLayoutFindingMode.setVisibility(View.INVISIBLE);
                        mLayoutStandaloneMode.setVisibility(View.INVISIBLE);
                        mLayoutAlreadyConnect.setVisibility(View.INVISIBLE);
                        mLayoutSearch.setVisibility(View.INVISIBLE);
                        mLayoutConnectingMode.setVisibility(View.INVISIBLE);
                        mLayoutDisconnected.setVisibility(View.VISIBLE);
                        mFindingAnimation.stop();
                        mConnectingAnimation.stop();
                        stopPowerLightAnimation();
                        break;
                }
            }
        });
    }


    /**
     * Start Power Light Animation
     */
    private void startPowerLightAnimation() {
        if (timerPowerLight != null) {
            return;
        }
        timerPowerLight = new Timer(true);
        timerPowerLight.schedule(new TimerTask() {
            int count = 0;

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (count % 2 == 0) {
                            AlphaAnimation alphaIncrease = new AlphaAnimation(0.2F, 1.0F);
                            alphaIncrease.setDuration(1500);
                            alphaIncrease.setFillAfter(true);
                            mPowerLightImageView.startAnimation(alphaIncrease);
                        } else {
                            AlphaAnimation alphaDecrease = new AlphaAnimation(1.0F, 0.2F);
                            alphaDecrease.setDuration(1500);
                            alphaDecrease.setFillAfter(true);
                            mPowerLightImageView.startAnimation(alphaDecrease);
                        }
                        count++;
                    }
                });
            }
        }, 0, 1550);
    }

    /**
     * Stop Power Light Animation
     */
    private void stopPowerLightAnimation() {
        if (timerPowerLight != null) {
            timerPowerLight.cancel();
            timerPowerLight = null;
        }
    }

    /**
     * BLE Callback
     **/
    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, int status, final int newState) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switch (newState) {
                        case BluetoothProfile.STATE_CONNECTED:
                            System.out.println("CONNECTED");
//						if (mIntMobileType == ConnectivityManager.TYPE_MOBILE){
//							ConnectionUtils.setWifiHotspot(BLEConnectionActivity.this, true);
//						}
                            switchUi(SWITCH_CONNECTION_BC310_UI);
                            mGatt.discoverServices();
                            break;
                        case BluetoothProfile.STATE_DISCONNECTED:
                            System.out.println("DISCONNECTED");
                            disconnectDevice();
                            switchUi(SWITCH_DISCONNECTED_BC310_UI);
                            break;
                    }
                }
            });
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                displayGattServices(gatt, gatt.getServices());
            }
        }

        /***** READ *****/
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (characteristic.getUuid().toString().equals(UUID_R_HEX_OCCUPY_STATE)) {
                byte[] occupyValue = characteristic.getValue();
                System.out.println("Read Occupy State " + bytes2Hex(occupyValue));
                if (occupyValue[0] == (byte) 0x00 || occupyValue[0] == (byte) 0xFF) {
                    readStreamState();
                } else if (occupyValue[0] == (byte) 0x01) {
                    // Read Occupying Device Name
                    readOccupyDevice();
                } else {
                    switchUi(SWITCH_DISCONNECTED_BC310_UI);
                }
            } else if (characteristic.getUuid().toString().equals(UUID_R_HEX_STREAM_STATE)) {
                byte[] streamStateValue = characteristic.getValue();
                System.out.println("Read Stream State " + bytes2Hex(streamStateValue));
                if (streamStateValue[0] == (byte) 0x00) {
                    // No Input Signal
                    mStringErrorMessage = "**** No Signal Input ****";
//					finishActivity();
                } else if (streamStateValue[1] == (byte) 0x00 || streamStateValue[1] == (byte) 0x01) {
                    // Stream Error (Red Light) - Set Occupy Device
                    // Stream Ready (Blue Light)
                    writeOccupyDevice(Build.MODEL);
                } else if (streamStateValue[1] == (byte) 0x02) {
                    // Show Stand-alone Mode Layout - Read Live Url
                    new Timer(true).schedule(new TimerTask() {
                        @Override
                        public void run() {
                            readCdnType();
                        }
                    }, 1500);
                } else {
                    switchUi(SWITCH_DISCONNECTED_BC310_UI);
                }
            } else if (characteristic.getUuid().toString().equals(UUID_RN_IP)) {
                System.out.println("Read IP " + mStringBc310ClientIp);
                mStringBc310ClientIp = characteristic.getStringValue(0);
                isBc310ClientStreaming = true;
//				finishActivity();
            } else if (characteristic.getUuid().toString().equals(UUID_R_OCC_DEVICE)) {
                mStringALDeviceName = characteristic.getStringValue(0);
                System.out.println("Occupy Device : " + mStringALDeviceName);
                switchUi(SWITCH_ALREADY_CONNECT_BC310_UI);
            } else if (characteristic.getUuid().toString().equals(UUID_R_HEX_CDN_TYPE)) {
                byte[] cndTypeValue = characteristic.getValue();
                System.out.println("cdn " + bytes2Hex(cndTypeValue));
//				if(cndTypeValue[0] == (byte)0x00){
//					showCdnType(CDNconfig.CDN_USTREAM);
//				} else if(cndTypeValue[0] == (byte)0x01){
//					showCdnType(CDNconfig.CDN_YOUTUBE);
//				} else if(cndTypeValue[0] == (byte)0x02){
//					showCdnType(CDNconfig.CDN_LIVE_HOUSE_IN);
//				} else if(cndTypeValue[0] == (byte)0x03){
//					showCdnType(CDNconfig.CDN_OTHER);
//				} else {
//					showCdnType(CDNconfig.CDN_OTHER);
//				}
                new Timer(true).schedule(new TimerTask() {
                    @Override
                    public void run() {
                        readLiveUrl_1();
                    }
                }, 1000);
            } else if (characteristic.getUuid().toString().equals(UUID_R_LIVE_URL_1)) {
                System.out.println(bytes2Hex(characteristic.getValue()));
                mStringCdnUrl = characteristic.getStringValue(0);
                System.out.println(mStringCdnUrl + " " + characteristic.getStringValue(0).length());
                if (characteristic.getStringValue(0).length() >= 16) {
                    new Timer(true).schedule(new TimerTask() {
                        @Override
                        public void run() {
                            readLiveUrl_2();
                        }
                    }, 1000);
                } else {
                    switchUi(SWITCH_STANDALONE_BC310_UI);
                }
            } else if (characteristic.getUuid().toString().equals(UUID_R_LIVE_URL_2)) {
                mStringCdnUrl += characteristic.getStringValue(0);
                System.out.println(mStringCdnUrl + " " + characteristic.getStringValue(0).length());
                if (characteristic.getStringValue(0).length() >= 16) {
                    new Timer(true).schedule(new TimerTask() {
                        @Override
                        public void run() {
                            readLiveUrl_3();
                        }
                    }, 1000);
                } else {
                    switchUi(SWITCH_STANDALONE_BC310_UI);
                }
            } else if (characteristic.getUuid().toString().equals(UUID_R_LIVE_URL_3)) {
                mStringCdnUrl += characteristic.getStringValue(0);
                System.out.println(mStringCdnUrl + " " + characteristic.getStringValue(0).length());
                if (characteristic.getStringValue(0).length() >= 16) {
                    new Timer(true).schedule(new TimerTask() {
                        @Override
                        public void run() {
                            readLiveUrl_4();
                        }
                    }, 1000);
                } else {
                    switchUi(SWITCH_STANDALONE_BC310_UI);
                }
            } else if (characteristic.getUuid().toString().equals(UUID_R_LIVE_URL_4)) {
                mStringCdnUrl += characteristic.getStringValue(0);
                System.out.println(mStringCdnUrl + " " + characteristic.getStringValue(0).length());
                if (characteristic.getStringValue(0).length() >= 16) {
                    new Timer(true).schedule(new TimerTask() {
                        @Override
                        public void run() {
                            readLiveUrl_5();
                        }
                    }, 1000);
                } else {
                    switchUi(SWITCH_STANDALONE_BC310_UI);
                }
            } else if (characteristic.getUuid().toString().equals(UUID_R_LIVE_URL_5)) {
                mStringCdnUrl += characteristic.getStringValue(0);
                System.out.println(mStringCdnUrl + " " + characteristic.getStringValue(0).length());
                switchUi(SWITCH_STANDALONE_BC310_UI);
            }
        }

        /***** WRITE *****/
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (characteristic.getUuid().toString().equals(UUID_RW_REG_OCC_DEVICE)) {
                System.out.println("Write Device " + characteristic.getStringValue(0));
            } else if (characteristic.getUuid().toString().equals(UUID_RW_SET_SSID_1)) {
                System.out.println("Write SSID 1 " + bytes2Hex(characteristic.getValue()));
                if (hex2Decimal(bytes2Hex(characteristic.getValue()).substring(0, 2)) > 16) {
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            mGatt.writeCharacteristic(characteristicMap.get(UUID_RW_SET_SSID_2));
                        }
                    }, 2000);
                }
            } else if (characteristic.getUuid().toString().equals(UUID_RW_SET_SSID_2)) {
                System.out.println("Write SSID 2 " + bytes2Hex(characteristic.getValue()));
            } else if (characteristic.getUuid().toString().equals(UUID_RW_SET_PW_1)) {
                System.out.println("Write PW 1 ");
                mIntPwLength = hex2Decimal(bytes2Hex(characteristic.getValue()).substring(0, 2));
                // Write AP Password or Protocol
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (mIntPwLength > 16) {
                            mGatt.writeCharacteristic(characteristicMap.get(UUID_RW_SET_PW_2));
                        } else {
                            if (mIntMobileType == ConnectivityManager.TYPE_MOBILE) {
                                writeApSecurity(mIntHotspotSecurity, mIntMobileType);
                            } else {
                                writeApSecurity(mIntApSecurity, mIntMobileType);
                            }
                        }
                    }
                }, 2000);
            } else if (characteristic.getUuid().toString().equals(UUID_RW_SET_PW_2)) {
                System.out.println("Write PW 2 ");
                // Write AP Password or Protocol
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (mIntPwLength > 32) {
                            mGatt.writeCharacteristic(characteristicMap.get(UUID_RW_SET_PW_3));
                        } else {
                            if (mIntMobileType == ConnectivityManager.TYPE_MOBILE) {
                                writeApSecurity(mIntHotspotSecurity, mIntMobileType);
                            } else {
                                writeApSecurity(mIntApSecurity, mIntMobileType);
                            }
                        }
                    }
                }, 2000);
            } else if (characteristic.getUuid().toString().equals(UUID_RW_SET_PW_3)) {
                System.out.println("Write PW 3 ");
                // Write AP Password or Protocol
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (mIntPwLength > 48) {
                            mGatt.writeCharacteristic(characteristicMap.get(UUID_RW_SET_PW_4));
                        } else {
                            if (mIntMobileType == ConnectivityManager.TYPE_MOBILE) {
                                writeApSecurity(mIntHotspotSecurity, mIntMobileType);
                            } else {
                                writeApSecurity(mIntApSecurity, mIntMobileType);
                            }
                        }
                    }
                }, 2000);
            } else if (characteristic.getUuid().toString().equals(UUID_RW_SET_PW_4)) {
                System.out.println("Write PW 4 ");
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (mIntMobileType == ConnectivityManager.TYPE_MOBILE) {
                            writeApSecurity(mIntHotspotSecurity, mIntMobileType);
                        } else {
                            writeApSecurity(mIntApSecurity, mIntMobileType);
                        }
                    }
                }, 2000);
            } else if (characteristic.getUuid().toString().equals(UUID_RW_SET_PROT)) {
                System.out.println("Write Protocol " + bytes2Hex(characteristic.getValue()));
            }
        }

        /***** NOTIFY *****/
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (characteristic.getUuid().toString().equals(UUID_N_DEC_WIFI_STATE)) {
                        int wifiState = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                        System.out.println("Nofify WiFi State " + wifiState);
                        if (wifiState == WIFI_STATE_SET_CONFIG_FAIL) {
                            switchUi(SWITCH_SEARCH_BC310_UI);
                        } else if (wifiState == WIFI_STATE_SSID_NOT_FOUND) {
                            // Input AP Password
                            if (mIntMobileType == ConnectivityManager.TYPE_MOBILE) {
//								new Timer(true).schedule(new TimerTask() {
//									@Override
//									public void run() {
//										if (mIntHotspotSecurity == ConnectionUtils.NONE){
//											writeApSecurity(mIntHotspotSecurity, mIntMobileType);
//										} else {
//											writeApPW(mStringHotspotPw);
//										}
//									}
//								}, 1500);
                            } else {
//								if (mIntApSecurity == ConnectionUtils.NONE){
//									new Timer(true).schedule(new TimerTask() {
//										@Override
//										public void run() {
//											writeApSecurity(mIntApSecurity, mIntMobileType);
//										}
//									}, 1500);
//								} else {
//									showApPwDialog(mStringApSsid, mIntApSecurity);
//								}
                            }
                        } else if (wifiState == WIFI_STATE_SSID_FOUND) {
                            // Notify IP
                        } else if (wifiState == WIFI_STATE_AUTH_FAIL) {
                            // AP Password Error
//							if (mIntMobileType == ConnectivityManager.TYPE_MOBILE){
//								new Timer(true).schedule(new TimerTask() {
//									@Override
//									public void run() {
//										if (mIntHotspotSecurity == ConnectionUtils.NONE){
//											writeApSecurity(mIntHotspotSecurity, mIntMobileType);
//										} else {
//											writeApPW(mStringHotspotPw);
//										}
//									}
//								}, 1500);
//							} else {
//								showApPwDialog(mStringApSsid, mIntApSecurity);
//								Toast.makeText(BLEConnectionActivity.this,
//										getString(R.string.bc310_connection_toast_pw_incorrect),
//										Toast.LENGTH_SHORT).show();
//							}
                        } else if (wifiState == WIFI_STATE_TIME_OUT) {
                            switchUi(SWITCH_SEARCH_BC310_UI);
                        } else if (wifiState == WIFI_STATE_SSID_CONNECTED) {
                            isBc310ClientStreaming = true;
                            new Timer(true).schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    readClientIp();
                                }
                            }, 1000);
                        } else {
                            switchUi(SWITCH_DISCONNECTED_BC310_UI);
                        }
                    } else if (characteristic.getUuid().toString().equals(UUID_RN_IP)) {
                        System.out.println("Nofify IP");
                        mStringBc310ClientIp = characteristic.getStringValue(0);
                        isBc310ClientStreaming = true;
//						new Timer(true).schedule(new TimerTask() {
//							@Override
//							public void run() {
//								finishActivity();
//							}
//						}, 5000);
                    } else if (characteristic.getUuid().toString().equals(UUID_RN_SESSION)) {
                        // Write AP SSID to BC310
                        new Timer(true).schedule(new TimerTask() {
                            @Override
                            public void run() {
                                if (mIntMobileType == ConnectivityManager.TYPE_MOBILE) {
                                    writeApSsid(mStringHotspotSsid);
                                } else {
                                    writeApSsid(mStringApSsid);
                                }
                            }
                        }, 1500);
                        mIntBc310SessionKey = Integer.valueOf(characteristic.getStringValue(0));
                        System.out.println("Nofify Session Key: ");
                    }
                }
            });
        }

        // Enable Notify Characteristic
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            if (descriptor.getCharacteristic().getUuid().toString().equals(UUID_N_DEC_WIFI_STATE)) {
                System.out.println("open notify wifi state");
                notifySessionKey();
            } else if (descriptor.getCharacteristic().getUuid().toString().equals(UUID_RN_SESSION)) {
                System.out.println("open notify session key");
                notifyClientIp();
            } else if (descriptor.getCharacteristic().getUuid().toString().equals(UUID_RN_IP)) {
                System.out.println("open notify ip");
                readOccupyState();
            }
        }
    };

    /**
     * Display BluetoothGattService
     *
     * @param gatt
     * @param gattServices
     */
    private void displayGattServices(BluetoothGatt gatt, final List<BluetoothGattService> gattServices) {
        characteristicMap.clear();
        for (BluetoothGattService gattService : gattServices) {
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
            for (final BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                characteristicMap.put(gattCharacteristic.getUuid().toString(), gattCharacteristic);
            }
        }

        new Timer(true).schedule(new TimerTask() {
            @Override
            public void run() {
                if (mGatt != null && gattServices != null) {
                    notifyWiFiState();
                } else {
                    switchUi(SWITCH_DISCONNECTED_BC310_UI);
                }
            }
        }, 1200);
    }

    /**
     * Get Power State

     private void readPowerState() {
     mGatt.readCharacteristic(characteristicMap.get(UUID_R_HEX_POWER_STATE));
     }
     */

    /**
     * Get Stream State
     */
    private void readStreamState() {
        if (mGatt != null) {
            mGatt.readCharacteristic(characteristicMap.get(UUID_R_HEX_STREAM_STATE));
        }
    }

    /**
     * Get Occupy State
     */
    private void readOccupyState() {
        if (mGatt != null) {
            mGatt.readCharacteristic(characteristicMap.get(UUID_R_HEX_OCCUPY_STATE));
        }
    }

    /**
     * Get Client Mode IP
     */
    private void readClientIp() {
        if (mGatt != null) {
            mGatt.readCharacteristic(characteristicMap.get(UUID_RN_IP));
        }
    }

    /**
     * Notify Client Mode IP
     */
    private void notifyClientIp() {
        if (mGatt != null) {
            mGatt.setCharacteristicNotification(characteristicMap.get(UUID_RN_IP), true);
            UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
            BluetoothGattDescriptor descriptor = characteristicMap.get(UUID_RN_IP).getDescriptor(CCCD);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mGatt.writeDescriptor(descriptor);
        }
    }

    /**
     * Notify WiFi State
     */
    private void notifyWiFiState() {
        if (mGatt != null) {
            mGatt.setCharacteristicNotification(characteristicMap.get(UUID_N_DEC_WIFI_STATE), true);
            UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
            BluetoothGattDescriptor descriptor = characteristicMap.get(UUID_N_DEC_WIFI_STATE).getDescriptor(CCCD);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mGatt.writeDescriptor(descriptor);
        }
    }

    /**
     * Write AP SSID
     *
     * @param ssid
     */
    private void writeApSsid(String ssid) {
        if (mGatt != null) {
            int ssidLength = ssid.length();
            byte[] ssid_total = ssid.getBytes();
            byte[] ssid_1;
            byte[] ssid_2;
            if (ssid.length() > 16) {
                ssid_1 = new byte[17];
                ssid_2 = new byte[1 + ssidLength - 16];
                // First byte total length
                ssid_1[0] = int2Bytes(ssidLength, 1)[0];
                // First byte 0x01
                ssid_2[0] = (byte) 0x01;
                // ssid_1 payload
                for (int i = 1; i <= 16; i++) {
                    ssid_1[i] = ssid_total[i - 1];
                }
                BluetoothGattCharacteristic characteristicSsid_1 = characteristicMap.get(UUID_RW_SET_SSID_1);
                characteristicSsid_1.setValue(ssid_1);
                mGatt.writeCharacteristic(characteristicSsid_1);
                // ssid_2 payload
                for (int i = 16; i < ssidLength; i++) {
                    ssid_2[i - 15] = ssid_total[i];
                }
                final BluetoothGattCharacteristic characteristicSsid_2 = characteristicMap.get(UUID_RW_SET_SSID_2);
                characteristicSsid_2.setValue(ssid_2);
                characteristicMap.put(UUID_RW_SET_SSID_2, characteristicSsid_2);
            } else {
                ssid_1 = new byte[1 + ssidLength];
                // First byte - total length
                ssid_1[0] = int2Bytes(ssidLength, 1)[0];
                // ssid_1 payload
                for (int i = 1; i <= ssidLength; i++) {
                    ssid_1[i] = ssid_total[i - 1];
                }
                BluetoothGattCharacteristic characteristicSsid_1 = characteristicMap.get(UUID_RW_SET_SSID_1);
                characteristicSsid_1.setValue(ssid_1);
                mGatt.writeCharacteristic(characteristicSsid_1);
            }
        }
    }

    /**
     * Write AP PW
     *
     * @param password
     */
    private void writeApPW(String password) {
        if (mGatt != null) {
            int pwLength = password.length();
            byte[] pw_total = password.getBytes();
            byte[] pw_1;
            byte[] pw_2;
            byte[] pw_3;
            byte[] pw_4;
            if (pwLength <= 16) {
                pw_1 = new byte[1 + pwLength];
                // First byte total length
                pw_1[0] = int2Bytes(pwLength, 1)[0];
                // PW Payload
                for (int i = 1; i <= pwLength; i++) {
                    pw_1[i] = pw_total[i - 1];
                }
                BluetoothGattCharacteristic characteristicPw_1 = characteristicMap.get(UUID_RW_SET_PW_1);
                characteristicPw_1.setValue(pw_1);
                mGatt.writeCharacteristic(characteristicPw_1);
            } else if (pwLength <= 32) {
                pw_1 = new byte[17];
                pw_2 = new byte[1 + pwLength - 16];
                // First byte total length
                pw_1[0] = int2Bytes(pwLength, 1)[0];
                pw_2[0] = (byte) 0x01;
                // PW1 Payload
                for (int i = 1; i <= 16; i++) {
                    pw_1[i] = pw_total[i - 1];
                }
                BluetoothGattCharacteristic characteristicPw_1 = characteristicMap.get(UUID_RW_SET_PW_1);
                characteristicPw_1.setValue(pw_1);
                mGatt.writeCharacteristic(characteristicPw_1);
                // PW2 Payload
                for (int i = 16; i < pwLength; i++) {
                    pw_2[i - 15] = pw_total[i];
                }
                final BluetoothGattCharacteristic characteristicPw_2 = characteristicMap.get(UUID_RW_SET_PW_2);
                characteristicPw_2.setValue(pw_2);
                characteristicMap.put(UUID_RW_SET_PW_2, characteristicPw_2);
            } else if (pwLength <= 48) {
                pw_1 = new byte[17];
                pw_2 = new byte[17];
                pw_3 = new byte[1 + pwLength - 32];
                // First byte total length
                pw_1[0] = int2Bytes(pwLength, 1)[0];
                pw_2[0] = (byte) 0x01;
                pw_3[0] = (byte) 0x02;
                // PW_1 Payload
                for (int i = 1; i <= 16; i++) {
                    pw_1[i] = pw_total[i - 1];
                }
                BluetoothGattCharacteristic characteristicPw_1 = characteristicMap.get(UUID_RW_SET_PW_1);
                characteristicPw_1.setValue(pw_1);
                mGatt.writeCharacteristic(characteristicPw_1);
                // PW_2 payload
                for (int i = 16; i < 32; i++) {
                    pw_2[i - 15] = pw_total[i];
                }
                final BluetoothGattCharacteristic characteristicPw_2 = characteristicMap.get(UUID_RW_SET_PW_2);
                characteristicPw_2.setValue(pw_2);
                characteristicMap.put(UUID_RW_SET_PW_2, characteristicPw_2);
                // PW_3 payload
                for (int i = 32; i < pwLength; i++) {
                    pw_3[i - 31] = pw_total[i];
                }
                final BluetoothGattCharacteristic characteristicPw_3 = characteristicMap.get(UUID_RW_SET_PW_3);
                characteristicPw_3.setValue(pw_3);
                characteristicMap.put(UUID_RW_SET_PW_3, characteristicPw_3);
            } else if (pwLength <= 64) {
                pw_1 = new byte[17];
                pw_2 = new byte[17];
                pw_3 = new byte[17];
                pw_4 = new byte[1 + pwLength - 48];
                // First byte total length
                pw_1[0] = int2Bytes(pwLength, 1)[0];
                pw_2[0] = (byte) 0x01;
                pw_3[0] = (byte) 0x02;
                pw_4[0] = (byte) 0x03;
                // PW_1 Payload
                for (int i = 1; i <= 16; i++) {
                    pw_1[i] = pw_total[i - 1];
                }
                BluetoothGattCharacteristic characteristicPw_1 = characteristicMap.get(UUID_RW_SET_PW_1);
                characteristicPw_1.setValue(pw_1);
                mGatt.writeCharacteristic(characteristicPw_1);
                // PW_2 payload
                for (int i = 16; i < 32; i++) {
                    pw_2[i - 15] = pw_total[i];
                }
                final BluetoothGattCharacteristic characteristicPw_2 = characteristicMap.get(UUID_RW_SET_PW_2);
                characteristicPw_2.setValue(pw_2);
                characteristicMap.put(UUID_RW_SET_PW_2, characteristicPw_2);
                // PW_3 payload
                for (int i = 32; i < 48; i++) {
                    pw_3[i - 31] = pw_total[i];
                }
                final BluetoothGattCharacteristic characteristicPw_3 = characteristicMap.get(UUID_RW_SET_PW_3);
                characteristicPw_3.setValue(pw_3);
                characteristicMap.put(UUID_RW_SET_PW_3, characteristicPw_3);
                // PW_4 payload
                for (int i = 48; i < pwLength; i++) {
                    pw_4[i - 47] = pw_total[i];
                }
                final BluetoothGattCharacteristic characteristicPw_4 = characteristicMap.get(UUID_RW_SET_PW_4);
                characteristicPw_4.setValue(pw_4);
                characteristicMap.put(UUID_RW_SET_PW_4, characteristicPw_4);
            }
        }
    }

    /**
     * Write WiFi Protocol
     */
    private void writeApSecurity(int protocol, int mobileType) {
        byte[] value = new byte[2];
//		if (protocol == ConnectionUtils.NONE){
//			value[0] = (byte)0x00;
//		} else if (protocol == ConnectionUtils.WEP_HEX10){
//			value[0] = (byte)0x01;
//		} else if (protocol == ConnectionUtils.WEP_HEX26){
//			value[0] = (byte)0x02;
//		} else if (protocol == ConnectionUtils.WEP_ASC5){
//			value[0] = (byte)0x03;
//		} else if (protocol == ConnectionUtils.WEP_ASC13){
//			value[0] = (byte)0x04;
//		} else if (protocol == ConnectionUtils.WPA){
//			value[0] = (byte)0x05;
//		}
        if (mobileType == ConnectivityManager.TYPE_WIFI) {
            value[1] = (byte) 0x00;
        } else {
            value[1] = (byte) 0x01;
        }
        BluetoothGattCharacteristic characteristicSecurity = characteristicMap.get(UUID_RW_SET_PROT);
        characteristicSecurity.setValue(value);
        mGatt.writeCharacteristic(characteristicSecurity);
    }

    /**
     * Read Occupy Device
     */
    private void readOccupyDevice() {
        if (mGatt != null) {
            mGatt.readCharacteristic(characteristicMap.get(UUID_R_OCC_DEVICE));
        }
    }

    /**
     * Write Occupy Device
     *
     * @param device
     */
    private void writeOccupyDevice(String device) {
        if (mGatt != null) {
            BluetoothGattCharacteristic characteristicDevice = characteristicMap.get(UUID_RW_REG_OCC_DEVICE);
            characteristicDevice.setValue(device.getBytes());
            mGatt.writeCharacteristic(characteristicDevice);
        }
    }

    /**
     * Read Live Url 1
     */
    private void readLiveUrl_1() {
        if (mGatt != null) {
            mGatt.readCharacteristic(characteristicMap.get(UUID_R_LIVE_URL_1));
        }
    }

    /**
     * Read Live Url 2
     */
    private void readLiveUrl_2() {
        if (mGatt != null) {
            mGatt.readCharacteristic(characteristicMap.get(UUID_R_LIVE_URL_2));
        }
    }

    /**
     * Read Live Url 3
     */
    private void readLiveUrl_3() {
        if (mGatt != null) {
            mGatt.readCharacteristic(characteristicMap.get(UUID_R_LIVE_URL_3));
        }
    }

    /**
     * Read Live Url 4
     */
    private void readLiveUrl_4() {
        if (mGatt != null) {
            mGatt.readCharacteristic(characteristicMap.get(UUID_R_LIVE_URL_4));
        }
    }

    /**
     * Read Live Url 5
     */
    private void readLiveUrl_5() {
        if (mGatt != null) {
            mGatt.readCharacteristic(characteristicMap.get(UUID_R_LIVE_URL_5));
        }
    }

    /**
     * Read CDN Type
     */
    private void readCdnType() {
        if (mGatt != null) {
            mGatt.readCharacteristic(characteristicMap.get(UUID_R_HEX_CDN_TYPE));
        }
    }

    /**
     * Notify Session Key
     */
    private void notifySessionKey() {
        if (mGatt != null) {
            mGatt.setCharacteristicNotification(characteristicMap.get(UUID_RN_SESSION), true);
            UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
            BluetoothGattDescriptor descriptor = characteristicMap.get(UUID_RN_SESSION).getDescriptor(CCCD);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mGatt.writeDescriptor(descriptor);
        }
    }

    private byte[] int2Bytes(int x, int n) {
        byte[] bytes = new byte[n];
        for (int i = 0; i < n; i++, x >>>= 8)
            bytes[i] = (byte) (x & 0xFF);
        return bytes;
    }

    // byte[] to String
    public String bytes2Hex(byte[] bytes) {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    // hex to int
    @SuppressLint("DefaultLocale")
    public static int hex2Decimal(String hex) {
        String digits = "0123456789ABCDEF";
        hex = hex.toUpperCase();
        int value = 0;
        for (int i = 0; i < hex.length(); i++) {
            char c = hex.charAt(i);
            int d = digits.indexOf(c);
            value = 16 * value + d;
        }
        return value;
    }

}