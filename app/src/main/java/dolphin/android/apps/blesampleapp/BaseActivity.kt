package dolphin.android.apps.blesampleapp

import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.util.Log

abstract class BaseActivity : Activity() {
    companion object {
        private const val TAG = "BaseActivity"
    }

    private var mService: Messenger? = null
    private var mBound = false

    //https://developer.android.com/guide/components/bound-services.html#Messenger
    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, service: IBinder) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            //Log.d("AppBaseActivity", "onServiceConnected")
            mService = Messenger(service)
            mBound = true
            sendMessageToBleService(0, Bundle().apply {
                putParcelable("receiver", mResultReceiver)
            })
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            //Log.d("AppBaseActivity", "onServiceDisconnected")
            mService = null
            mBound = false
        }
    }

    fun startBleService() {
        startService(Intent(this, BleCtrlService::class.java))
    }

    fun bindBleService() {
        bindService(Intent(this, BleCtrlService::class.java), mConnection, 0)
        //Context.BIND_AUTO_CREATE)
    }

    fun unbindBleService() {
        sendMessageToBleService(-1)
        unbindService(mConnection)
    }

    fun sendMessageToBleService(what: Int, data: Bundle? = null) {
        mService?.send(Message.obtain(null, what).apply { this.data = data })
    }

    private val mResultReceiver = object : ResultReceiver(Handler()) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
            //super.onReceiveResult(resultCode, resultData)
            //Log.d(TAG, String.format("resultCode = %d", resultCode));
            onReceiveMessengerResult(resultCode, resultData)
        }
    }

    protected open fun onReceiveMessengerResult(resultCode: Int, data: Bundle?) {
        Log.d(TAG, "onReceiveMessengerResult: $resultCode")
    }
}