package dolphin.android.apps.blesampleapp

import android.app.Service
import android.bluetooth.*
import android.content.Context
import android.content.Intent
import android.os.*
import android.util.Log
import java.lang.ref.WeakReference
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class BleCtrlService : Service() {
    companion object {
        private const val TAG = "BleCtrlService"
    }

    private lateinit var handler: IncomingMsgHandler
    private lateinit var messenger: Messenger
    private var resultReceiver: ResultReceiver? = null
    private lateinit var executor: ExecutorService

    private lateinit var mBluetoothAdapter: BluetoothAdapter
    private var mBluetoothGatt: BluetoothGatt? = null

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate")
        handler = IncomingMsgHandler(this)
        messenger = Messenger(handler)
        executor = Executors.newSingleThreadExecutor()

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        mBluetoothAdapter = (getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
    }

    override fun onBind(intent: Intent?): IBinder {
        return messenger.binder
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
    }

    class IncomingMsgHandler internal constructor(s: BleCtrlService) : Handler() {
        private val service = WeakReference(s)

        override fun handleMessage(msg: Message?) {
            service.get()?.let {
                if (!it.handleMessage(msg)) {
                    super.handleMessage(msg)
                }
            } ?: kotlin.run {
                super.handleMessage(msg)
            }
        }
    }

    internal fun sendMessageToActivity(what: Int, data: Bundle? = null) {
        resultReceiver?.send(what, data)
    }

    private fun handleMessage(msg: Message?): Boolean {
        when (msg?.what) {
            0 -> {
                resultReceiver = msg.data.getParcelable("receiver")
                return true
            }
            -1 -> {
                resultReceiver = null
                return true
            }

            1 -> {
                val address = msg.data.getString("address")
                Log.d(TAG, "connect to $address")
                executor.submit { connectGatt(address) }
                return true
            }
            2 -> {
                executor.submit { disconnectGatt() }
                return true
            }
        }
        return false
    }

    private fun connectGatt(address: String) {
        if (mBluetoothGatt != null) {
            mBluetoothGatt!!.disconnect()
            Log.v(TAG, "disconnect from previous GATT")
        }

        val device = mBluetoothAdapter.getRemoteDevice(address)
        if (device == null) {
            Log.e(TAG, "unable to find device: $address")
            return
        }

        mBluetoothGatt = device.connectGatt(this, false,
                //https://developer.android.com/reference/android/bluetooth/BluetoothGattCallback.html
                object : BluetoothGattCallback() {
                    override fun onReadRemoteRssi(gatt: BluetoothGatt?, rssi: Int, status: Int) {
                        super.onReadRemoteRssi(gatt, rssi, status)
                        Log.d(TAG, "onReadRemoteRssi: $rssi $status")
                    }

                    override fun onCharacteristicRead(gatt: BluetoothGatt?,
                                                      characteristic: BluetoothGattCharacteristic?,
                                                      status: Int) {
                        super.onCharacteristicRead(gatt, characteristic, status)
                        Log.d(TAG, "onCharacteristicRead: $status")
                    }

                    override fun onCharacteristicWrite(gatt: BluetoothGatt?,
                                                       characteristic: BluetoothGattCharacteristic?,
                                                       status: Int) {
                        super.onCharacteristicWrite(gatt, characteristic, status)
                        Log.d(TAG, "onCharacteristicWrite: status=$status")
                    }

                    override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
                        super.onServicesDiscovered(gatt, status)
                        Log.d(TAG, "onServicesDiscovered: status=$status")

                        gatt?.services?.forEach {
                            Log.d(TAG, "UUID: ${it.uuid}")
                            it.characteristics.forEach {
                                Log.d(TAG, "  characteristic: ${it.uuid}")
                            }
                        }
                    }

                    override fun onPhyUpdate(gatt: BluetoothGatt?, txPhy: Int, rxPhy: Int,
                                             status: Int) {
                        super.onPhyUpdate(gatt, txPhy, rxPhy, status)
                        Log.d(TAG, "onPhyUpdate: status=$status tx=$txPhy rx=$rxPhy")
                    }

                    override fun onMtuChanged(gatt: BluetoothGatt?, mtu: Int, status: Int) {
                        super.onMtuChanged(gatt, mtu, status)
                        Log.d(TAG, "onMtuChanged: status=$status mtu=$mtu")
                    }

                    override fun onReliableWriteCompleted(gatt: BluetoothGatt?, status: Int) {
                        super.onReliableWriteCompleted(gatt, status)
                        Log.d(TAG, "onReliableWriteCompleted: status=$status")
                    }

                    override fun onDescriptorWrite(gatt: BluetoothGatt?,
                                                   descriptor: BluetoothGattDescriptor?,
                                                   status: Int) {
                        super.onDescriptorWrite(gatt, descriptor, status)
                        Log.d(TAG, "onDescriptorWrite: status=$status")
                    }

                    override fun onCharacteristicChanged(gatt: BluetoothGatt?,
                                                         characteristic: BluetoothGattCharacteristic?) {
                        super.onCharacteristicChanged(gatt, characteristic)
                        Log.d(TAG, "onCharacteristicChanged")
                    }

                    override fun onDescriptorRead(gatt: BluetoothGatt?,
                                                  descriptor: BluetoothGattDescriptor?, status: Int) {
                        super.onDescriptorRead(gatt, descriptor, status)
                        Log.d(TAG, "onDescriptorRead: status=$status")
                    }

                    override fun onPhyRead(gatt: BluetoothGatt?, txPhy: Int, rxPhy: Int,
                                           status: Int) {
                        super.onPhyRead(gatt, txPhy, rxPhy, status)
                        Log.d(TAG, "onPhyRead: status=$status tx=$txPhy rx=$rxPhy")
                    }

                    override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int,
                                                         newState: Int) {
                        super.onConnectionStateChange(gatt, status, newState)
                        Log.d(TAG, "onConnectionStateChange: $status $newState")
                        if (newState == BluetoothProfile.STATE_CONNECTED) {
                            Log.i(TAG, "Connected to GATT server. ${gatt?.device?.address}")
                            //Log.d(TAG, "GATT: $gatt")
                            val r = gatt?.discoverServices()
                            Log.i(TAG, "Attempting to start service discovery: $r")
                        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                            Log.i(TAG, "Disconnected from GATT server. ${gatt?.device?.address}")
                            disconnectGatt()
                        }
                    }
                })
    }

    private fun disconnectGatt() {
        mBluetoothGatt?.disconnect()
        mBluetoothGatt?.close()
        mBluetoothGatt = null
    }
}