package dolphin.android.apps.blesampleapp

import android.annotation.TargetApi
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.widget.SwipeRefreshLayout
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class ScanActivity : BaseActivity(), AdapterView.OnItemClickListener {
    companion object {
        private const val TAG = "ScanActivity"

        // Stops scanning after 10 seconds.
        private const val SCAN_PERIOD: Long = 10000
    }

    private lateinit var mBluetoothAdapter: BluetoothAdapter
    private var mBleScanCallback: Any? = null
    private var mScanning: Boolean = false
    private val mDeviceList = ArrayList<BluetoothDevice>()

    private lateinit var mListView: ListView
    private lateinit var mSwipeRefreshLayout: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        // Checks if Bluetooth is supported on the device.
        if (bluetoothManager.adapter == null) {
            Toast.makeText(this, R.string.bluetooth_not_supported, Toast.LENGTH_SHORT).show()
            finish()
            return
        }

        mBluetoothAdapter = bluetoothManager.adapter

        setContentView(R.layout.activity_scan)
        mListView = findViewById(android.R.id.list)
        mListView.onItemClickListener = this

        mSwipeRefreshLayout = findViewById(android.R.id.custom)
        mSwipeRefreshLayout.setOnRefreshListener { startScanBle() }
    }

    override fun onResume() {
        super.onResume()

        bindBleService()
        if (!mBluetoothAdapter.isEnabled) {
            Log.e(TAG, "bluetooth is not enabled")
            Toast.makeText(this, R.string.bluetooth_not_enabled, Toast.LENGTH_SHORT).show()
        } else {
            startScanBle()
        }
    }

    override fun onPause() {
        super.onPause()
        stopScanBle()
        sendMessageToBleService(2)
        unbindBleService()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.scan, menu)
        return true //super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_scan -> {
                startScanBle()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    private fun startScanBle() {
        if (mScanning) {
            Log.w(TAG, "already scanning... don't start again")
            return
        }
        if (!mBluetoothAdapter.isEnabled) {
            Log.e(TAG, "Bluetooth is not enabled yet")
            return
        }

        //set a timer to stop scan
        Handler().postDelayed({ stopScanBle() }, SCAN_PERIOD)

        Log.d(TAG, "start scan BLE devices")
        mScanning = true
        runOnUiThread { mSwipeRefreshLayout.isRefreshing = true }
        mDeviceList.clear()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //https://developer.android.com/reference/android/bluetooth/le/ScanCallback.html
            mBleScanCallback = object : ScanCallback() {
                override fun onScanFailed(errorCode: Int) {
                    super.onScanFailed(errorCode)
                    Log.e(TAG, "onScanFailed: $errorCode")
                }

                //https://developer.android.com/reference/android/bluetooth/le/ScanResult.html
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                override fun onScanResult(callbackType: Int, result: ScanResult?) {
                    super.onScanResult(callbackType, result)
                    Log.d(TAG, "onScanResult: $callbackType $result")
                    if (result?.device != null) {
                        mDeviceList.add(result.device)
                        notifyDeviceListChanged()
                    }
                }

                override fun onBatchScanResults(results: MutableList<ScanResult>?) {
                    super.onBatchScanResults(results)
                    Log.d(TAG, "onBatchScanResults: $results")
                }
            }

            mBluetoothAdapter.bluetoothLeScanner.startScan(mBleScanCallback as ScanCallback)
        } else {
            mBluetoothAdapter.startLeScan(mLeScanCallback)
        }
    }

    private fun stopScanBle() {
        if (!mScanning) {
            Log.e(TAG, "already stopped")
            return
        }
        if (!mBluetoothAdapter.isEnabled) {
            Log.e(TAG, "Bluetooth is not enabled yet")
            return
        }

        Log.d(TAG, "stop scan BLE devices")
        mScanning = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBluetoothAdapter.bluetoothLeScanner.stopScan(mBleScanCallback as ScanCallback)
        } else {
            mBluetoothAdapter.stopLeScan(mLeScanCallback)
        }
        //notifyDeviceListChanged()
        runOnUiThread { mSwipeRefreshLayout.isRefreshing = false }
    }

    private val mLeScanCallback = BluetoothAdapter.LeScanCallback { device, rssi, scanRecord ->
        Log.d(TAG, "found device: ${device.address} $rssi ${device.name}")
        mDeviceList.add(device)
        notifyDeviceListChanged()
    }

    private fun notifyDeviceListChanged() {
        //Log.d(TAG, "list size: ${mDeviceList.size}")
        runOnUiThread {
            mListView.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1,
                    Array(mDeviceList.size, {
                        //Log.d(TAG, "$it: ${mDeviceList[it].name}")
                        mDeviceList[it].address + " " + mDeviceList[it].name
                    }))
        }
    }

    override fun onItemClick(listView: AdapterView<*>?, v: View?, position: Int, id: Long) {
        Log.d(TAG, "click $position")
        stopScanBle() //don't scan... it may change the list
        sendMessageToBleService(1, Bundle().apply {
            putString("address", mDeviceList[position].address)
        })
    }
}
